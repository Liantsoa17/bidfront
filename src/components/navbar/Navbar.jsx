import React from 'react';
import { GiHamburgerMenu } from 'react-icons/gi';
import { MdOutlineRestaurantMenu } from 'react-icons/md';
import images from '../../constants/images';
import './navbar.css';

const Navbar = () => {
  const [toggleMenu, setToggleMenu] = React.useState(false);
  return (
    <nav className="app__navbar">
      <div className="app__navbar-logo">
        <img src={images.gericht} alt="app__logo" />
      </div>
      <ul className="app__navbar-links">
        <li className="p__opensans"><a href="/listeenchere">Tous les enchères</a></li>
        <li className="p__opensans"><a href="/mesenchere">Mes enchères</a></li>
        <li className="p__opensans"><a href="/encherefait">Enchères fait</a></li>
      </ul>
      <div className="app__navbar-login">
        
      </div>
      <div className="app__navbar-smallscreen">
        <GiHamburgerMenu color="#fff" fontSize={27} onClick={() => setToggleMenu(true)} />
        {toggleMenu && (
          <div className="app__navbar-smallscreen_overlay flex__center slide-bottom">
            <MdOutlineRestaurantMenu fontSize={27} className="overlay__close" onClick={() => setToggleMenu(false)} />
            <ul className="app__navbar-smallscreen_links">
              <li><a href="/listeenchere" onClick={() => setToggleMenu(false)}>Tous les enchères</a></li>
              <li><a href="/mesenchere" onClick={() => setToggleMenu(false)}>Mes enchères</a></li>
              <li><a href="/encherefait" onClick={() => setToggleMenu(false)}>Enchères fait</a></li>
             
            </ul>
          </div>
        )}
      </div>
    </nav>
  );
};

export default Navbar;
