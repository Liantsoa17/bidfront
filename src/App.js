import React from 'react';

import { MesEnchere,Rencherir,EnchereFait,Historique,Login,AboutUs, Chef, Footer, Gallery, Header, Intro, Laurels, ListEnchere, SpecialMenu,FooterOverlay,NewsLetter } from './container';
import { BrowserRouter,Routes,Route} from 'react-router-dom';
import { Navbar } from './components';
import './App.css';

const App = () => (
  <div>
    <Navbar/>
    <BrowserRouter>
        <Routes>
        <Route path='/' element={<Header/>}></Route>
        <Route path='/header' element={<Header/>}></Route>
          <Route path='/login' element={<Login/>}></Route>
          <Route path='/listeenchere' element={<ListEnchere/>}></Route>
          <Route path='/mesenchere' element={<MesEnchere/>}></Route>
          <Route path='/historique/:bidid' element={<Historique/>}></Route>
          <Route path='/rencherir/:bidid' element={<Rencherir/>}></Route>
          <Route path='/encherefait' element={<EnchereFait/>}></Route>
        </Routes>
        </BrowserRouter>
   <FooterOverlay></FooterOverlay>
    
  </div>
);

export default App;
