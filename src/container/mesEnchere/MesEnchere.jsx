import {React,useEffect,useState} from 'react';
import { Link, useNavigate } from "react-router-dom";
import './MesEnchere.css';
const MesEnchere = () => {
    const[bidlist,bidlistchange]=useState(null);
    const navigate = useNavigate();



    var token = localStorage.getItem('token');
    const header=new Headers();
    header.set('token', token);
    console.log(token);
   
    useEffect(() => {
      if(token==null){
        navigate("/login")
    }
    fetch("http://encherebackend-production-21bf.up.railway.app/utilisateur/checktoken",{
        method:"POST",
        headers: {
          'Content-Type': 'application/json',
          'token': token
        }
    }).then((resu)=>{
      if(resu==null){
        navigate("/login")
      }
   })
    },[]);
    const LoadDetail = (id) => {
        navigate("/historique/" + id);
    }

   
    useEffect(()=>{
        
        fetch("http://encherebackend-production-21bf.up.railway.app/mesencheres",{
            method:"POST",
            headers: {
            'Content-Type': 'application/json',
            'token': token
            }
        }).then((res)=>{
             return res.json();
        }).then((resp)=>{
                bidlistchange(resp);
        }).catch((err)=>{
            console.log(err.message)
        })
    },[])
    return(
        <div class="all">
        {bidlist &&
            bidlist.map(item=>(
                <div class="card">
            <div class="card-image">
                <img src={item.photo.base64} alt="Image" />
            </div>
            <div class="card-content">
                <h2>{item.nom}</h2>
                <h4>{(item.date_enchere).replace("T", " ")}</h4>
                <p>{item.description}</p>
                <br></br>
                <u><a onClick={() => { LoadDetail(item.id) }} className="btn btn-primary">Voir historique</a></u>
            </div>
        </div>
            ))
    
      }
      </div>
    );
}
export default MesEnchere;
