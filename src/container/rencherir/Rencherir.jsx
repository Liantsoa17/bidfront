import { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import SubHeading from '../SubHeading/SubHeading';

import './Rencherir.css';

const Rencherir = () => {
    const { bidid } = useParams();
    const navigate=useNavigate();
    const idEnchere=bidid;
    const[input,inputchange]=useState();
    

    var token = localStorage.getItem('token');
    var tek=sessionStorage.getItem('token');
    const header=new Headers();
    header.set('token', token);
    
   
    useEffect(() => {
      if(token==null){
        navigate("/login")
    }
    console.log(tek);
    fetch("http://encherebackend-production-21bf.up.railway.app/utilisateur/checktoken",{
        method:"POST",
        headers: {
          'Content-Type': 'application/json',
          'token': token
        }
    }).then((resu)=>{
      if(resu==null){
        navigate("/login")
      }
   })
    },[]);

 

    const handlesubmit=(e)=>{
        e.preventDefault();
        const inputEncherir={input,idEnchere};
      
  
        fetch("http://encherebackend-production-21bf.up.railway.app/rencherir",{
          method:"POST",
          headers: {
            'Content-Type': 'application/json',
            'token': token
          },
          body:JSON.stringify(inputEncherir)
        }).then((res)=>{
            return res.json();
        }).then((resu)=>{
            alert(resu);
            navigate("/");
        })
        .catch((err)=>{
          console.log(err.message)
        })
  
      }
      return(
        <form className="container" onSubmit={handlesubmit}>
        <div className="app__newsletter">
            <div className="app__newsletter-heading">
            <SubHeading title="ajouter votre mise" />
            <h1 className="headtext__cormorant">Rencherir</h1>
            <p className="p__opensans">Ajout nouveau</p>
            </div>
            <div className="app__newsletter-input flex__center">
                <input type="number" placeholder="input" name='input' value={input}  onChange={e=>inputchange(e.target.value)} />
            </div>
            <br />
            <center><button type="submit" className="custom__button">Valider</button></center>
            
        </div>
    </form>
      )

}
export default Rencherir;