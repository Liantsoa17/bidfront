import React from 'react';

import SubHeading from '../SubHeading/SubHeading';
import './Newsletter.css';

const Newsletter = () => (
  <div className="app__newsletter">
    <div className="app__newsletter-heading">
      <SubHeading title="Rechercher" />
    </div>
    <div className="app__newsletter-input flex__center">
    <input type="Date" placeholder="date" name='date' /><br /><br />
        <select name="categorie" id="" class="nomena">
          <option value="1">Categorie</option>
        </select>
    </div>
    <div className="app__newsletter-input flex__center">
      <select name="statut" id="" class="nomena">
          <option value="1">Statut</option>
        </select><br /><br />
      <input type="number" placeholder="prix" name='prix' />
    </div><br />
    <center><button type="button" className="custom__button">Valider</button></center>
      <br></br>
  </div>
);

export default Newsletter;
