import React from 'react';

import { SubHeading } from '../../components';
import { images } from '../../constants';
import './Header.css';

const Header = () => (
  <div className="app__header app__wrapper section__padding" id="home">
    <div className="app__wrapper_info">
      <SubHeading title="Chercher des objets uniques" />
      <h1 className="app__header-h1">Bid on Vintage, Own a Piece of History.</h1>
      <p className="p__opensans" style={{ margin: '2rem 0' }}>"Notre site d'enchères en ligne vous offre une plateforme unique pour découvrir et acquérir des objets uniques et précieux. Avec une grande sélection de produits allant des antiquités rares aux objets de collection modernes, vous êtes sûr de trouver quelque chose qui répond à vos intérêts. </p>
      <button type="button" className="custom__button"><a href='/listeenchere'>Voir liste des enchères</a></button>
    </div>

    <div className="app__wrapper_img">
      <img src={images.appareil} alt="header_img" />
    </div>
  </div>
);

export default Header;
