import { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";

import SubHeading from '../SubHeading/SubHeading';
import './Login.css';

const Login = () => {
    const navigate=useNavigate();
   

    const[email,setEmail]=useState("");
    const[mdp,setMdp]=useState("");

    function saveTokenOnStorage(token) {
        localStorage.setItem('token', token);
    }
    function removeTokenOnStorage() {
        localStorage.removeItem('token');
    }


    const handlesubmit=(e)=>{
        e.preventDefault();
        const login={mdp,email};
        
  
        fetch("http://encherebackend-production-21bf.up.railway.app/utilisateur/login",{
          method:"POST",
          headers:{"content-type":"application/json"},
          body:JSON.stringify(login)
        }).then(res=>res.json().then((data=>{
                if(data==null){
                    alert('Sign in failed '+data.error.code + "-" + data.error.message);
                }
                else{
                    console.log(data);
                    removeTokenOnStorage();
                    saveTokenOnStorage(data.value);
                    navigate('/listeenchere')
                }
        }))).catch((error) => {
            alert('Sign in failed '+error);
        })
          
  
      }
    return(
        <form className="container" onSubmit={handlesubmit}>
            <div className="app__newsletter">
                <div className="app__newsletter-heading">
                <SubHeading title="log in" />
                <h1 className="headtext__cormorant">Login pour enchère</h1>
                <p className="p__opensans">email1:jean@gmail.com  mdp1:root  email2:nono@gmail.com  mdp:root</p>
                </div>
                <div className="app__newsletter-input flex__center">
                    <input type="text" placeholder="Email" name='email' value={email}  onChange={e=>setEmail(e.target.value)} />
                </div>
                <div className="app__newsletter-input flex__center">
                <input type="password" placeholder="password" name='mdp' value={mdp}  onChange={e=>setMdp(e.target.value)} />
                </div><br />
                <center><button type="submit" className="custom__button">Se connecter</button></center>
                
            </div>
        </form>
   )
      };

export default Login;