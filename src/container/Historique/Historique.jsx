import React from 'react';
import { BsInstagram, BsArrowLeftShort, BsArrowRightShort } from 'react-icons/bs';

import { SubHeading } from '../../components';
import { images } from '../../constants';
import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";

import './Historique.css'
const Historique = () => {
    const scrollRef = React.useRef(null);
    var [photos, setphotos] = useState(null);

  const scroll = (direction) => {
    const { current } = scrollRef;

    if (direction === 'left') {
      current.scrollLeft -= 300;
    } else {
      current.scrollLeft += 300;
    }
  };
  useEffect(()=>{
    fetch("http://encherebackend-production-21bf.up.railway.app/getPhotoParEnchere/"+bidid).then((res)=>{
         return res.json();
    }).then((liste)=>{
        setphotos(liste);
        console.log("ok ok ok ok o k ok ok ok ");
    }).catch((err)=>{
        console.log(err.message)
    })
},[])
    const { bidid } = useParams();
    const[bidlist,bidlistchange]=useState(null);
    useEffect(()=>{
        fetch("http://encherebackend-production-21bf.up.railway.app/historique/"+bidid).then((res)=>{
             return res.json();
        }).then((resp)=>{
                bidlistchange(resp);
        }).catch((err)=>{
            console.log(err.message)
        })
    },[])
 
    return(
        <div className="app__gallery"><div className="app__gallery flex__center">
      <div className="app__gallery-content">
        <SubHeading title="Historique" />
        <h1 className="headtext__cormorant">Images</h1>
        <div className="app__gallery-images_container">
       
           
         
        </div>
        <p className="p__opensans" style={{ color: '#AAAAAA', marginTop: '2rem' }}></p>
        <button type="button" className="custom__button"></button><br /><br />
      </div>
      <div className="app__gallery-images">
        <div className="app__gallery-images_container" ref={scrollRef}>
          { photos &&
          photos.map((image) => (
            <div className="app__gallery-images_card flex__center" >
              <img src={image.base64} alt="gallery_image" />
             
            </div>
          ))}
        </div>
        <div className="app__gallery-images_arrows">
          <BsArrowLeftShort className="gallery__arrow-icon" onClick={() => scroll('left')} />
          <BsArrowRightShort className="gallery__arrow-icon" onClick={() => scroll('right')} />
        </div>
      </div><br />
    </div>
    <div>
    <table class="tableau-style" border="1" width="600" height="0">
      <thead>
        <tr>
            <th>Nom</th>
            <th>Description</th>
           <th>Date enchere</th>
        </tr>
      </thead>
      <tbody>
      { bidlist &&
            bidlist.map(item=>(
                <tr key={item.id}>
                    <td>{item.utilisateur.nom}</td>
                    <td>{item.prix_enchere}</td>
                    <td>{item.dateEncherir}</td>
                </tr>
            ))
        }
      </tbody>
        
      </table>
    </div>
    </div>

        
    );

};
export default Historique;
