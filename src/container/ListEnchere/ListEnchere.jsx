import {React,useEffect,useState} from 'react';
import { NewsLetter } from '..';
import { images } from '../../constants';
import SubHeading from '../SubHeading/SubHeading';

import { Link, useNavigate } from "react-router-dom";
import './ListeEnchere.css';

const ListEnchere = () => {
    const[bidlist,bidlistchange]=useState(null);
    const[categorie,setcategorie]=useState(null);
    const[statut,setstatut]=useState(null);


    const[idCategorie,setselectcat]=useState("");
    const[idStatut,setselectstat]=useState("");
    const[prix,setprix]=useState("");
    const[date_debut,setdebut]=useState("");
    const[mot_cle,setmot]=useState("");


    const navigate = useNavigate();
    const LoadDetail = (id) => {
        navigate("/rencherir/" + id);
    }

    
    var token = localStorage.getItem('token');
    const header=new Headers();
    header.set('token', token);

    const handlesubmit=(e)=>{
        e.preventDefault();
        
        const search={date_debut,idCategorie,idStatut,prix,mot_cle};
      
  
        fetch("http://encherebackend-production-21bf.up.railway.app/rechercher",{
          method:"POST",
          headers: {
            'Content-Type': 'application/json',
            'token': token
          },
          body:JSON.stringify(search)
        }).then((resultat)=>{
            return resultat.json();
        }).then((result)=>{
            bidlistchange(result);
        }).catch((err)=>{
          console.log(err.message)
        })
        fetch("http://encherebackend-production-21bf.up.railway.app/categorie").then((res)=>{
            return res.json();
       }).then((resp)=>{
               setcategorie(resp);
       }).catch((err)=>{
           console.log(err.message)
       })
       fetch("http://encherebackend-production-21bf.up.railway.app/statutenchere").then((res)=>{
            return res.json();
       }).then((resp)=>{
               setstatut(resp);
       }).catch((err)=>{
           console.log(err.message)
       })
  
      }


    useEffect(()=>{
        fetch("http://encherebackend-production-21bf.up.railway.app/enchere").then((res)=>{
             return res.json();
        }).then((resp)=>{
                bidlistchange(resp);
        }).catch((err)=>{
            console.log(err.message)
        })
        fetch("http://encherebackend-production-21bf.up.railway.app/categorie").then((res)=>{
             return res.json();
        }).then((resp)=>{
                setcategorie(resp);
        }).catch((err)=>{
            console.log(err.message)
        })
        fetch("http://encherebackend-production-21bf.up.railway.app/statutenchere").then((res)=>{
             return res.json();
        }).then((resp)=>{
                setstatut(resp);
        }).catch((err)=>{
            console.log(err.message)
        })
        
    },[])

   return(
    <div class="all">
         <div className="app__newsletter">
    <div className="app__newsletter-heading">
      <SubHeading title="Rechercher" />
    </div>
    <form onSubmit={handlesubmit}>
    <div className="app__newsletter-input flex__center">
    <input value={date_debut} type="datetime-local" onChange={e=>setdebut(e.target.value)} placeholder="date" name='date' /><br /><br />
    <select value={idCategorie} onChange={e=>setselectcat(e.target.value)} class="nomena">
                            <option  key={0} value={0} >categorie</option>
                                { categorie &&
                                    categorie.map(cate=>(
                                        <option key={cate.id} value={cate.id}>{cate.nom}</option>
                                    ))

                                }
                            
                        </select>
    </div>
    <div className="app__newsletter-input flex__center">
    <select value={idStatut} onChange={e=>setselectstat(e.target.value)} class="nomena">
                            <option  key={0} value={0} >categorie</option>
                                { statut &&
                                    statut.map(stat=>(
                                        <option key={stat.id} value={stat.id}>{stat.intitule}</option>
                                    ))

                                }
                            
    </select><br /><br />
      <input value={prix} type="number" onChange={e=>setprix(e.target.value)}  placeholder="prix" name='prix' />
    </div >
    <div className="app__newsletter-input flex__center">
        <input value={mot_cle} type="text" onChange={e=>setmot(e.target.value)}  placeholder="mot cle" name='mot cle' class="nomena" />
        <br /><br />
        <button type="submit" className="custom__button">Rechercher</button>
    </div>
   
      <br></br>
      </form>
  </div>
  {bidlist &&
        bidlist.map(item=>(
            <div class="card">
        <div class="card-image">
            <img src={item.photo.base64} alt="Image" />
        </div>
        <div class="card-content">
            <h3>{item.nom}</h3>
            <h4>{(item.date_enchere).replace("T", " ")}</h4>
            <p>{item.description}</p>
            <br></br>
            <u><a onClick={() => { LoadDetail(item.id) }} className="btn btn-primary">Rencherir</a></u>
        </div>
    </div>
        ))

  }
   
       
        </div>
    );
    
   
   
}
export default ListEnchere;