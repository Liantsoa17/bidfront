import {React,useEffect,useState} from 'react';
import { Link, useNavigate, useParams } from "react-router-dom";
import './EnchereFait.css';
const Encherefait = () => {
    const navigate=useNavigate();
    var token = localStorage.getItem('token');
    var tek=sessionStorage.getItem('token');
    const header=new Headers();
    header.set('token', token);
    
   
    useEffect(() => {
      if(token==null){
        navigate("/login")
    }
    console.log(tek);
    fetch("http://encherebackend-production-21bf.up.railway.app/utilisateur/checktoken",{
        method:"POST",
        headers: {
          'Content-Type': 'application/json',
          'token': token
        }
    }).then((resu)=>{
      if(resu==null){
        navigate("/login")
      }
   })
    },[]);

    const[bidlist,bidlistchange]=useState(null);
    useEffect(()=>{
        fetch("http://encherebackend-production-21bf.up.railway.app/encherefait",{
            method:"GET",
            headers: {
            'Content-Type': 'application/json',
            'token': token
            }
        }).then((res)=>{
             return res.json();
        }).then((resp)=>{
                bidlistchange(resp);
        }).catch((err)=>{
            console.log(err.message)
        })
    },[])
    return(
        <div className="app__gallery">
           
            <table class="tableau-style" border="1" width="600" height="0">
      <thead>
        <tr>
          <td>Nom</td>
          <td>Description</td>
          <td>Date enchere</td>
          <td>prix</td>
        </tr>
      </thead>
      <tbody>
      { bidlist &&
                        bidlist.map(item=>(
                          
                                <tr key={item.enchere.id}>
                                    <td>{item.enchere.nom}</td>
                                    <td>{item.enchere.description}</td>
                                    <td>{(item.enchere.date_enchere).replace("T"," ")}</td>
                                    <td>{item.encherir.prix_encherir}</td>
                                </tr>
                            
                        ))
                    }
       
      </tbody>
        
      </table>
        </div>
    )

}
export default Encherefait;